Pod::Spec.new do |s|
  s.name = 'PetClient'
  s.ios.deployment_target = '10.0'
  s.osx.deployment_target = '10.11'
  s.tvos.deployment_target = '9.0'
  s.version = '0.0.1'
  s.source = { :git => 'https://perrygupta@bitbucket.org/perrygupta/petclient.git', :tag => 'v1.0.0' }
  s.authors = 'Swagger Codegen'
  s.license = 'Proprietary'
  s.source_files = 'PetClient/Classes/**/*.swift'
  s.dependency 'Alamofire', '~> 5.1.0'
end
