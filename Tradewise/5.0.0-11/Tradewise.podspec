Pod::Spec.new do |s|
    #s.source_files = '*.swift'
    s.name = 'Tradewise'
    s.authors = 'Yonas Kolb'
    s.summary = 'Trade Wisely, Trade Smart'
    s.version = '5.0.0-11'
    s.homepage = 'https://github.com/yonaskolb/SwagGen'
    s.source = { :git => 'https://@bitbucket.org/codepharmacy2/tradewise-sdk-swift.git' }
    s.ios.deployment_target = '9.0'
    s.tvos.deployment_target = '9.0'
    s.osx.deployment_target = '10.9'
    #s.source_files = 'Sources/**/*.swift'
    s.static_framework = true
    s.dependency 'Alamofire', '~> 4.9.0'
    s.subspec 'Sources' do |ss|
    s.source_files = 'Sources/*.swift'
    end
    s.subspec 'Models' do |ss|
    ss.source_files = 'Sources/Models/*.swift'
    end
    s.subspec 'Requests' do |ss|
    ss.source_files = 'Sources/Requests/**/*.swift'
    end
    s.subspec 'Coin' do |ss|
    ss.source_files = 'Sources/Coin/*.swift'
    end
    s.subspec 'Credit' do |ss|
    ss.source_files = 'Sources/Credit/*.swift'
    end
    s.subspec 'Credit' do |ss|
    ss.source_files = 'Sources/Credit/*.swift'
    end
    s.subspec 'FavoriteCoin' do |ss|
    ss.source_files = 'Sources/FavoriteCoin/*.swift'
    end
    s.subspec 'Key' do |ss|
    ss.source_files = 'Sources/Key/*.swift'
    end
    s.subspec 'Notification' do |ss|
    ss.source_files = 'Sources/Notification/*.swift'
    end
    s.subspec 'NotificationPreference' do |ss|
    ss.source_files = 'Sources/NotificationPreference/*.swift'
    end
    s.subspec 'Order' do |ss|
    ss.source_files = 'Sources/Order/*.swift'
    end
    s.subspec 'WalletCoin' do |ss|
    ss.source_files = 'Sources/WalletCoin/*.swift'
    end
end
