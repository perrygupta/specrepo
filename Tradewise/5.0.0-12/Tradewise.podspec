Pod::Spec.new do |s|
    #s.source_files = '*.swift'
    s.name = 'Tradewise'
    s.authors = 'Yonas Kolb'
    s.summary = 'Trade Wisely, Trade Smart'
    s.version = '5.0.0-12'
    s.homepage = 'https://github.com/yonaskolb/SwagGen'
    s.source = { :git => 'https://@bitbucket.org/codepharmacy2/tradewise-sdk-swift.git' }
    s.ios.deployment_target = '9.0'
    s.tvos.deployment_target = '9.0'
    s.osx.deployment_target = '10.9'
    #s.source_files = 'Sources/**/*.swift'
    s.static_framework = true
    s.dependency 'Alamofire', '~> 4.9.0'
    s.subspec 'Sources' do |ss|
    s.source_files = 'Sources/*.swift'
    end
    s.subspec 'Models' do |ss|
    ss.source_files = 'Sources/Models/*.swift'
    end
    s.subspec 'Requests' do |ss|
    ss.source_files = 'Sources/Requests/**/*.swift'
    end
    s.subspec 'Requests/Coin' do |ss|
    ss.source_files = 'Sources/Requests/Coin/*.swift'
    end
    s.subspec 'Requests/CreditInfo' do |ss|
    ss.source_files = 'Sources/Requests/CreditInfo/*.swift'
    end
    s.subspec 'Requests/FavoriteCoin' do |ss|
    ss.source_files = 'Sources/Requests/FavoriteCoin/*.swift'
    end
    s.subspec 'Requests/Key' do |ss|
    ss.source_files = 'Sources/Requests/Key/*.swift'
    end
    s.subspec 'Requests/Notification' do |ss|
    ss.source_files = 'Sources/Requests/Notification/*.swift'
    end
    s.subspec 'Requests/NotificationPreference' do |ss|
    ss.source_files = 'Sources/Requests/NotificationPreference/*.swift'
    end
    s.subspec 'Requests/Order' do |ss|
    ss.source_files = 'Sources/Requests/Order/*.swift'
    end
    s.subspec 'Requests/WalletCoin' do |ss|
    ss.source_files = 'Sources/Requests/WalletCoin/*.swift'
    end
end
