Pod::Spec.new do |s|
    s.source_files = '*.swift'
    s.name = 'Tradewise'
    s.authors = 'Yonas Kolb'
    s.summary = 'Trade Wisely, Trade Smart'
    s.version = '5.0.0-10'
    s.homepage = 'https://github.com/yonaskolb/SwagGen'
    s.source = { :git => 'https://@bitbucket.org/codepharmacy2/tradewise-sdk-swift.git' }
    s.ios.deployment_target = '9.0'
    s.tvos.deployment_target = '9.0'
    s.osx.deployment_target = '10.9'
    s.source_files = 'Sources/**/*.swift'
    s.dependency 'Alamofire', '~> 4.9.0'
end
